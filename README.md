# README #

Conky is a free, light-weight system monitor for X, that displays any information on your desktop.

### My Arch Conky ###

* This conky config is generated for a resolution of 1600x900.
* Version 0.2.3

### Install ###

* Download latest version from [Downloads section](https://bitbucket.org/laegnur/my-arch-conky/downloads)
* Extract it to your home folder
* The archive contains a **.font** and a **.conky** folders, plus a **.conky_start.sh** file.
* The **.font** folder includes all used font files.
* The **.conky** folder contains all the config files, including a **lua** scripts sub-folder.
* The **.conky_start.sh** is the starting script. You need to add it to your starting apps.