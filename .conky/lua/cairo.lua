--[[
#=======================================================================
# Date    : 2014/12/08
# Author  : Laegnur
# Version : v0.2.3
#=======================================================================
]]

require 'cairo'

--[[
#=======================================================================
#Anillos
#=======================================================================
]]
rings = {
{
  sensor="${exec sensors | grep -A 0 'Core 0' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=50, y=245,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${cpu cpu1}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0xFFFFFF,
  fg_alpha=1,
  x=50, y=245,
  radius=34,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${exec sensors | grep -A 0 'Core 1' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=130, y=245,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${cpu cpu2}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0xFFFFFF,
  fg_alpha=1,
  x=130, y=245,
  radius=34,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${exec sensors | grep -A 0 'Core 2' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=210, y=245,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${cpu cpu3}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0xFFFFFF,
  fg_alpha=1,
  x=210, y=245,
  radius=34,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${exec sensors | grep -A 0 'Core 3' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=290, y=245,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${cpu cpu4}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0xFFFFFF,
  fg_alpha=1,
  x=290, y=245,
  radius=34,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${execi 5 sensors | grep -A 0 'GPU' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=50, y=495,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${execi 5 sensors | grep -A 0 'temp1' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=170, y=495,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
{
  sensor="${execi 5 sensors | grep -A 0 'temp2' | cut -c16-19}",
  max=100,
  bg_colour=0xFFFFFF,
  bg_alpha=0.5,
  fg_colour=0x1793D1,
  fg_alpha=1,
  x=290, y=495,
  radius=30,
  thickness=2,
  start_angle=-180,
  end_angle=180
},
}

--[[
#=======================================================================
#Variables
#=======================================================================
]]

corner_r = 30
bg_colour = 0x4D4D4D
bg_alpha = 0.9

--[[
#=======================================================================
#RGB a R, G, B
#=======================================================================
]]
function rgb_to_r_g_b(colour,alpha)
  return ((colour / 0x10000) % 0x100) / 255., ((colour / 0x100) % 0x100) / 255., (colour % 0x100) / 255., alpha
end

--[[
#=======================================================================
#Dibujar Fondo
#=======================================================================
]]
function conky_draw_bg()
  if conky_window==nil then
    return ' '
  end

  local h=conky_window.height
  local w=conky_window.width
  local cs=cairo_xlib_surface_create(conky_window.display, conky_window.drawable, conky_window.visual, w, h)

  cr=cairo_create(cs)
  cairo_move_to(cr,corner_r,0)
  cairo_line_to(cr,w-corner_r,0)
  cairo_curve_to(cr,w,0,w,0,w,corner_r)
  cairo_line_to(cr,w,h-corner_r)
  cairo_curve_to(cr,w,h,w,h,w-corner_r,h)
  cairo_line_to(cr,corner_r,h)
  cairo_curve_to(cr,0,h,0,h,0,h-corner_r)
  cairo_line_to(cr,0,corner_r)
  cairo_curve_to(cr,0,0,0,0,corner_r,0)
  cairo_close_path(cr)
  cairo_set_source_rgba(cr,rgb_to_r_g_b(bg_colour,bg_alpha))
  cairo_fill(cr)
  return ' '
end

--[[
#=======================================================================
#Dibujar Anillo
#=======================================================================
]]
function conky_draw_ring(display, t, ring)
  local w,h=conky_window.width,conky_window.height
  local xc,yc,ring_r,ring_w,sa,ea=ring['x'],ring['y'],ring['radius'],ring['thickness'],ring['start_angle'],ring['end_angle']
  local bgc, bga, fgc, fga=ring['bg_colour'], ring['bg_alpha'], ring['fg_colour'], ring['fg_alpha']
  local angle_0=sa*(2*math.pi/360)-math.pi/2
  local angle_f=ea*(2*math.pi/360)-math.pi/2
  local t_arc=t*(angle_f-angle_0)

  cairo_arc(cr,xc,yc,ring_r,angle_0,angle_f)
  cairo_set_source_rgba(cr,rgb_to_r_g_b(bgc,bga))
  cairo_set_line_width(cr,ring_w)
  cairo_stroke(cr)
  cairo_arc(cr,xc,yc,ring_r,angle_0,angle_0+t_arc)
  cairo_set_source_rgba(cr,rgb_to_r_g_b(fgc,fga))
  cairo_stroke(cr)
end

--[[
#=======================================================================
#Anillos
#=======================================================================
]]
function conky_rings()
  if conky_window==nil then
    return ' '
  end

  local cs=cairo_xlib_surface_create(conky_window.display,conky_window.drawable,conky_window.visual, conky_window.width,conky_window.height)
  local display = cairo_create(cs)
  local updates = conky_parse('${updates}')

  local function get_rings(display, ring)
    local str = ''
    local value = 0
        
    str=conky_parse(ring['sensor'])
    value=tonumber(str)
    pct=value/ring['max']
    conky_draw_ring(display,pct,ring)
  end

  update_num = tonumber(updates)
  if update_num > 5 then
    for i in pairs(rings) do
      get_rings(display, rings[i])
    end
  end
  cairo_surface_destroy(cs)
  cairo_destroy(display)
  return ' '
end
